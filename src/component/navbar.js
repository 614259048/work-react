import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
export default class Navbar extends Component {
  render() {
    return (
      <div className="container posi-nav text-xl navbg shadow">
        <ul className="nav justify-content-center p-3">

          <li className="nav-item">
            <a className="nav-link tx-dark " href="#">Actives</a>
          </li>
          <li className="nav-item">
            <a className="nav-link tx-dark" href="#">Link</a>
          </li>
          <li className="nav-item">
            <a className="nav-link tx-dark" href="#">Link</a>
          </li>
          <li className="nav-item">
            <a className="nav-link tx-dark " href="#">Disabled</a>
          </li>
          <li className="nav-item">
            <a className="nav-link tx-dark " href="#">Active</a>
          </li>
          <li className="nav-item">
            <a className="nav-link tx-dark" href="#">Link</a>
          </li>
          <li className="nav-item">
            <a className="nav-link tx-dark" href="#">Link</a>
          </li>
          <li className="nav-item">
            <a className="nav-link tx-dark " href="#">Disabled</a>
          </li>
        </ul>


      </div>
    )
  }
}
