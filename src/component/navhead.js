import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/navh.css'
import NavItem from './NavItem';
export default class Navhead extends Component {
  constructor(props) {
    super(props);
    this.state = {
      'NavItemActive': ''
    }
  }
  render() {
    return (
      <div className=" posi-navh sticky-top">
        <div>
          <nav className="navbar navbar-expand-lg navh-bg navbar-dark">
            <a className="navbar-brand" href="#">Navbar</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item active">
                <NavItem item="Home" tolink="/"></NavItem>
                </li>
                <li className="nav-item">
                <NavItem item="Register" tolink="/Register"></NavItem>
                </li>
                <li className="nav-item">
                <NavItem item="Regisfreelance" tolink="/regisfreelance"></NavItem>
                </li>
                <li className="nav-item ">
                <NavItem item="Login" tolink="/login"></NavItem>
                </li>
                <li className="nav-item">
                <NavItem item="Profile" tolink="/profile"></NavItem>
                </li>
                <li className="nav-item">
                <NavItem item="Testpor" tolink="/Testpro"></NavItem>
                </li>
               
              </ul>
              <form className="form-inline my-2 my-lg-0">
                <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                <button className="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
              </form>
            </div>
          </nav>
        </div>

      </div>

    )
  }
}
