import React, { Component } from 'react'

export class Personal extends Component {
    render() {
        return (
            <div className=" mt-2 ml-5 mr-5 mb-3">
                <h4>Name</h4><br/>
                <label>Man</label><br/>
                <h4>Address</h4><br/>
                <label>34/2 M.2</label><br/>
                <h4>E-mail</h4><br/>
                <label>Mail@mail.com</label><br/>
                <h4>Phone</h4><br/>
                <label>0999999999</label><br/>
                <h4>ID card </h4><br/>
                <label>1234567890111</label><br/>
                <h4>dd/mm/yy(birth)</h4><br/>
                <label>01/01/2020</label><br/>
                <button className="btn btn-primary btn-block mt-3" type="button" data-bs-toggle="modal" data-bs-target="#personal" data-bs-whatever="@getbootstrap">Edit</button>

                <div className="modal fade" id="personal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">New message</h5>
                                <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div className="modal-body">
                                <form>
                                    <div className="mb-3">
                                        <label for="recipient-name" className="col-form-label">Name:</label>
                                        <input type="text" className="form-control" id="recipient-name" />
                                    </div>
                                    <div className="mb-3">
                                        <label for="message-text" className="col-form-label">Address:</label>
                                        <textarea className="form-control" id="message-text"></textarea>
                                    </div>
                                    <div className="mb-3">
                                        <label for="recipient-name" className="col-form-label">E-mail:</label>
                                        <input type="text" className="form-control" id="recipient-name" />
                                    </div>
                                    <div className="mb-3">
                                        <label for="recipient-name" className="col-form-label">Phone:</label>
                                        <input type="number" className="form-control" id="recipient-name" maxLength="10"/>
                                    </div>
                                    <div className="mb-3">
                                        <label for="recipient-name" className="col-form-label">ID card:</label>
                                        <input type="number" className="form-control" id="recipient-name" maxLength="13"/>
                                    </div>
                                    <div className="mb-3">
                                        <label for="recipient-name" className="col-form-label">dd/mm/yy(birth):</label>
                                        <input type="date" className="form-control" id="recipient-name" />
                                    </div>
                                    
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                <button type="button" className="btn btn-primary">Send message</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Personal;
