import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../../css/link.css'
import bgemp from '../../img/emp.jpg'
export default class NavEmp extends Component {
    render() {
        return (
            <div id={this.props.item} class="card bg-dark text-white">
                <Link to={this.props.tolink}>
                    <img class="card-img" src={bgemp} alt="Card image" />
                    <div class="card-img-overlay" >
                    <h5 class="card-title text-center" > สมัครผู้ว่าจ้าง </h5>
                    <p class="card-text" > 1.</p>
                    <p class="card-text" > 2.</p>
                    <p class="card-text" > 3.</p>
                    <p class="card-text" > 4.</p>
                    <p class="card-text" > Last updated 3 mins ago </p>

                </div>
                </Link>
            </div>
            
        )
    }
}

