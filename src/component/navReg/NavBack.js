import React, { Component } from 'react';
import {Link} from 'react-router-dom';
export default class NavBack extends Component {
    render() {
        return (
          
                <Link to={this.props.tolink}><button className="btn btn-light mt-2">ย้อนกลับ</button></Link>
          
        )
    }
}
