import React, { Component } from 'react'
import Personal from './personal'
import Work from './work'
import Review from './review'
import '../css/prof.css'
export default class Infomation extends Component {
    render() {
        return (
            <div className="mb-5 mt-5 ">
                <ul className="nav nav-pills mb-3 nav-justified" id="pills-tab" role="tablist">
                    <li className="nav-item">
                        <a className="nav-link active" id="pills-PERSONAL-tab" data-toggle="pill" href="#pills-PERSONAL" role="tab" aria-controls="pills-PERSONAL" aria-selected="true">PERSONAL</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" id="pills-WORK-tab" data-toggle="pill" href="#pills-WORK" role="tab" aria-controls="pills-WORK" aria-selected="false">WORK</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" id="pills-REVIEW-tab" data-toggle="pill" href="#pills-REVIEW" role="tab" aria-controls="pills-REVIEW" aria-selected="false">REVIEW</a>
                    </li>
                </ul>
                <div className="tab-content m-5" id="pills-tabContent">
                    <div className="tab-pane fade show active" id="pills-PERSONAL" role="tabpanel" aria-labelledby="pills-PERSONAL-tab">
                        <Personal />
                    </div>
                    <div className="tab-pane fade" id="pills-WORK" role="tabpanel" aria-labelledby="pills-WORK-tab">
                        <Work />
                    </div>
                    <div className="tab-pane fade" id="pills-REVIEW" role="tabpanel" aria-labelledby="pills-REVIEW-tab">
                        <Review />
                    </div>
                </div>
            </div>
        )
    }
}
