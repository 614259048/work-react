import React, { Component } from 'react';
import NavItem from '../NavItem';
class PersonalInfo extends Component {
    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    }

    render() {
        const { firstName, lastName, handleChange, IDstu, password, cofpassword } = this.props;
        return (
            <>


                {/*  */}
                <h3 className="mb-3">กรอกข้อมูลส่วนตัว</h3>
                <div class="form-row">
                    <div class="form-group col-md">
                        <label for="inputName4">รหัสนักศึกษา</label>
                        <input
                            type="int"
                            class="form-control"
                            id="inputName4"
                            name="IDstu"
                            value={IDstu}
                            placeholder="รหัสนักศึกษา"
                            onChange={handleChange('IDstu')}
                            maxLength="9"
                            ref={(input) => this.getidstu = input} />
                    </div>
                    <span id="idstu" className="error"></span>
                </div>
                {/*  */}
                {/*  */}
                <div class="form-row">
                    <div class="form-group col-md">
                        <label for="inputName4">ขื่อ</label>
                        <input
                            type="text"
                            class="form-control"
                            id="inputName4"
                            name="firstName"
                            value={firstName}
                            placeholder="ชื่อ"
                            onChange={handleChange('firstName')} />
                    </div>
                    <div class="form-group col-md">
                        <label for="inputName4">นามสกุล</label>
                        <input
                            type="text"
                            class="form-control"
                            id="inputName4"
                            name="lastName"
                            value={lastName}
                            placeholder="นามสกุล"
                            onChange={handleChange('lastName')} />
                    </div>
                </div>
                {/*  */}
                {/*  */}
                <div class="form-row">
                    <div class="form-group col-md">
                        <label for="inputName4">รหัสผ่าน</label>
                        <input
                            type="password"
                            class="form-control"
                            id="password"
                            name="password"
                            value={password}
                            placeholder="รหัสผ่าน"
                            onChange={handleChange('password')} />
                    </div>
                    <div class="form-group col-md">
                        <label for="inputName4">ยืนยันรหัสผ่าน</label>
                        <input
                            type="password"
                            class="form-control"
                            id="cofpassword"
                            name="cofpassword"
                            value={cofpassword}
                            placeholder="ยืนยันรหัสผ่าน"
                            onChange={handleChange('cofpassword')} />
                    </div>
                </div>
                {/*  */}
                <div className="btn btn-group btn-sm float-right">
                    <NavItem tolink="/" />
                    <button type="submit" class="btn btn-primary float-right mt-2" onClick={this.continue}>ยืนยันข้อมูล</button>
                </div>



            </>
        );
    }
}

export default PersonalInfo;