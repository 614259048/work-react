import React, { Component } from 'react';

class JobDetails extends Component {
    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    }

    back = e => {
        e.preventDefault();
        this.props.prevStep();
    }

    render() {
        const {handleChange, IDstu,otp } = this.props;
        const email = "@webmail.npru.ac.th";
        return (
            <>
               
                            
                                {/*  */}
                                <h3 className="mb-3">การยืนยันตัวตน</h3>

                                {/*  */}
                                {/*  */}
                                <div class="form-row">
                                    <div class="form-group col-md">
                                        <label for="inputName4">อีเมล</label>
                                        <input
                                            type="email"
                                            class="form-control"
                                            id="inputName4"
                                            name="email"
                                            value={IDstu + email}
                                            onChange={handleChange('IDstu')}
                                            disabled />
                                    </div>
                                    <div class="form-group col-md">
                                        <label for="inputName4">รหัส OTP</label>
                                        <input
                                            type="password"
                                            class="form-control"
                                            id="otp"
                                            name="otp"
                                            value={otp}
                                            placeholder="เลข OTP 4 ตัว"
                                            onChange={handleChange('otp')} />
                                    </div>
                                </div>
                                <div className="btn btn-group btn-sm float-right">
                                    <button className="Back  btn-light btn" onClick={this.back}>
                                        ย้อนกลับ
                                    </button>
                                    <button className="Next  btn-primary btn" onClick={this.continue}>
                                        ยืนยัน OTP
                                    </button>
                                </div>
                               
                    
            </>
        );
    }
}

export default JobDetails;