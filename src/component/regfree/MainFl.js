import React, { Component } from 'react';
import PersonalInfo from './PersonalInfo';
import JobDetails from './JobDetails';
import AllInfo from './AllInfo';
import bgfree from '../../img/free.jpg'

export class StepForm extends Component {
    state = {
        step: 1,

        // step 1
        firstName: '',
        lastName: '',
        email: '',

        // step 2
        jobTitle: '',
        jobCompany: '',
        jobLocation: ''

    }



    nextStep = () => {
        const { step } = this.state;

        this.setState({
            step: step + 1
        });
    }

    prevStep = () => {
        const { step } = this.state;
        this.setState({
            step: step - 1
        });
    }

    handleChange = input => e => {
        this.setState({ [input]: e.target.value });
    }

    showStep = () => {
        const { step, firstName, lastName, IDstu, password } = this.state;

        if (step === 1)
            return (<PersonalInfo
                nextStep={this.nextStep}
                handleChange={this.handleChange}
                firstName={firstName}
                lastName={lastName}
                IDstu={IDstu}
                password={password}
            />);
        if (step === 2)
            return (<JobDetails
                nextStep={this.nextStep}
                prevStep={this.prevStep}
                handleChange={this.handleChange}
                firstName={firstName}
                lastName={lastName}
                IDstu={IDstu}
                password={password}

            />);
        if (step === 3)
            return (<AllInfo
                firstName={firstName}
                lastName={lastName}
                IDstu={IDstu}
                password={password}
                prevStep={this.prevStep}
            />);
    }

    render() {
        const { step } = this.state;

        return (
            <>
                <div className="container text-white">

                    <div className="card">
                        <img class="card-img" src={bgfree} alt="Card image" />
                        <div className="card-img-overlay ">
                            <div class="card-header text-center bg-danger p-4"><h4>สมัครฟรีแลนซ์</h4> </div>
                            <h5 className="text-center text-dark mt-3"><b> ขั้นที่ {step} จาก 3</b></h5>
                            <form className="m-3 p-3 card bgtext"  name="regis" >
                                {this.showStep()}
                            </form>

                        </div>
                    </div>
                </div>


            </>
        );
    }
}

export default StepForm;