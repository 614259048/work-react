import React, { Component } from 'react'
import NavItem from './NavItem';
export default class navprof extends Component {
    render() {
        return (
            <div className=" posi-navprof">

                <nav className="navbar navbar-expand-lg navh-bg ">
                  
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav nav-link btn">
                            <li className="nav-item">
                                <NavItem item="Personal" tolink="/personal"></NavItem>
                            </li>
                            <li className="nav-item">
                                <NavItem item="Work" tolink="/work"></NavItem>
                            </li>
                
                        </ul>
                    </div>
                   
                </nav>


            </div>
        )
    }
}
