import React, { Component } from 'react';
import NavItem from '../NavItem';
class PersonalInfo extends Component {
    continue = e => {
        e.preventDefault();
        this.props.nextStep();
    }

    render() {
        const { firstName, lastName, handleChange, IDstu, password, cofpassword, email, userName } = this.props;
        return (
            <>


                {/*  */}
                <h3 className="mb-3">กรอกข้อมูลส่วนตัว</h3>
                <div class="form-row">
                    <div class="form-group col-md">
                        <label for="inputName4">อีเมล</label>
                        <input
                            type="email"
                            class="form-control"
                            id="inputName4"
                            name="email"
                            value={email}
                            placeholder="mail@mail"
                            onChange={handleChange('email')}
                        />
                    </div>
                    <div class="form-group col-md">
                        <label for="inputName4">ชื่อผู้ใช้</label>
                        <input
                            type="text"
                            class="form-control"
                            id="inputName4"
                            name="userName"
                            value={userName}
                            placeholder="ชื่อผู้ใช้"
                            onChange={handleChange('userName')}
                        />
                    </div>
                </div>
                {/*  */}
                {/*  */}
                <div class="form-row">
                    <div class="form-group col-md">
                        <label for="inputName4">ขื่อ</label>
                        <input
                            type="text"
                            class="form-control"
                            id="inputName4"
                            name="firstName"
                            value={firstName}
                            placeholder="ชื่อ"
                            onChange={handleChange('firstName')} />
                    </div>
                    <div class="form-group col-md">
                        <label for="inputName4">นามสกุล</label>
                        <input
                            type="text"
                            class="form-control"
                            id="inputName4"
                            name="lastName"
                            value={lastName}
                            placeholder="นามสกุล"
                            onChange={handleChange('lastName')} />
                    </div>
                </div>
                {/*  */}
                {/*  */}
                <div class="form-row">
                    <div class="form-group col-md">
                        <label for="inputName4">รหัสผ่าน</label>
                        <input
                            type="password"
                            class="form-control"
                            id="password"
                            name="password"
                            value={password}
                            placeholder="รหัสผ่าน"
                            onChange={handleChange('password')} />
                    </div>
                    <div class="form-group col-md">
                        <label for="inputName4">ยืนยันรหัสผ่าน</label>
                        <input
                            type="password"
                            class="form-control"
                            id="cofpassword"
                            name="cofpassword"
                            value={cofpassword}
                            placeholder="ยืนยันรหัสผ่าน"
                            onChange={handleChange('cofpassword')} />
                    </div>
                </div>
                {/*  */}
                <div className="btn btn-group btn-sm float-right">
                    <NavItem tolink="/" />
                    <button type="submit" class="btn btn-primary float-right mt-2" onClick={this.continue}>ยืนยันข้อมูล</button>
                </div>



            </>
        );
    }
}

export default PersonalInfo;