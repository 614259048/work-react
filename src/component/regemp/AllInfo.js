import React, { Component } from 'react';

class AllInfo extends Component {
    back = e => {
        e.preventDefault();
        this.props.prevStep();
    }
    render() {
        const { firstName, lastName, password, email,userName} = this.props;
        // const email = "@webmail.npru.ac.th";
        return (
            <>
                
                                {/*  */}
                                <div className="text-op">
                                <h3 className="mb-3">ยืนยันการสมัคร</h3>
                                <ul>
                                    <li>ชื่อ : {firstName}</li>
                                    <li>นามสกุล : {lastName}</li>
                                    <li>อีเมล : { email}</li>
                                    <li>ชื่อผู้ใช้ : {userName}</li>
                                    <li>รหัสผ่าน : {password}</li>
                                </ul>
                                {/*  */}
                                {/*  */}


                                <div className="btn btn-group btn-sm float-right">
                                    <button className="Back  btn-light btn" onClick={this.back}>
                                        ย้อนกลับ
                                    </button>
                                    <button className="Next  btn-primary btn" onClick={this.continue}>
                                        ยืนยัน OTP
                                    </button>
                                </div>
                                </div>
                    
                
               
            </>
        );
    }
}

export default AllInfo;