import React, { Component } from 'react';
import PersonalInfo from './PersonalInfo';
import JobDetails from './JobDetails';
import AllInfo from './AllInfo';
import bgemp from '../../img/emp.jpg'


export class StepForm1 extends Component {
    state = {
        step: 1,

        // step 1
        firstName: '',
        lastName: '',
        email: '',

        // step 2
        jobTitle: '',
        jobCompany: '',
        jobLocation: ''

    }

    nextStep = () => {
        const { step } = this.state;

        this.setState({
            step: step + 1
        });
    }

    prevStep = () => {
        const { step } = this.state;
        this.setState({
            step: step - 1
        });
    }

    handleChange = input => e => {
        this.setState({ [input]: e.target.value });
    }

    showStep = () => {
        const { step, firstName, lastName, IDstu, password, email,userName } = this.state;

        if (step === 1)
            return (<PersonalInfo
                nextStep={this.nextStep}
                handleChange={this.handleChange}
                firstName={firstName}
                lastName={lastName}
                email={email}
                password={password}
                userName={userName}
            />);
        if (step === 2)
            return (<JobDetails
                nextStep={this.nextStep}
                prevStep={this.prevStep}
                handleChange={this.handleChange}
                firstName={firstName}
                lastName={lastName}
                email={email}
                password={password}
                userName={userName}

            />);
        if (step === 3)
            return (<AllInfo
                firstName={firstName}
                lastName={lastName}
                email={email}
                password={password}
                userName={userName}
                prevStep={this.prevStep}
            />);
    }

    render() {
        const { step } = this.state;

        return (
            <>
                <div className="container text-white">

                    <div className="card">
                        <img class="card-img" src={bgemp} alt="Card image" />
                        <div className="card-img-overlay">
                            <div class="card-header text-center bg-danger p-4"><h4>สมัครผู้ว่างจ้าง</h4> </div>
                            <h5 className="text-center text-dark cb mt-3"> <b> ขั้นที่ {step} จาก 3</b></h5>
                            <form className="m-3 p-3 card bgtext" name="regis" >
                                {this.showStep()}
                            </form>

                        </div>
                    </div>
                </div>



            </>
        );
    }
}

export default StepForm1;