import React from 'react';

const Menu = ({ items }) => {

  return (
    <div className="section-center">
      {items.map((menuItems) => 
        {
          const { id, username, password, fname, lname, email, tel, img, type, workname, workdetail, worktype, workprice } = menuItems;
            if (items.type === "personal") {
              return (
                <article key={id} className="menu-item">
                  <img src={img} alt={null} className="photo" />
                  <div className="item-info">
                    <header>
                      <h4>{username}</h4>
                      <h4 className="tel">{fname}</h4>
                    </header>
                    <p className="item-text">{lname}</p>
                  </div>
                </article>
              )
            } else {
              return (
                <article key={id} className="menu-item">
                  <img src={img} alt={null} className="photo" />
                  <div className="item-info">
                    <header>
                      <h4>{workname}</h4>
                      <h4 className="tel">{workprice}</h4>
                    </header>
                    <p className="item-text">{worktype}</p>
                  </div>
                </article>
              )
            }
          }
        )
      }
    </div>
  );
};

export default Menu;
