import React, { Component } from 'react'
import logo from '../img/logo192.png';
export default class Review extends Component {
    render() {
        return (
            <div>
                <div className="card-group ">
                    <div className="card m-2 card-shadow">
                        <div className="row">
                            <div className="card-img col-2">
                                <img src={logo} alt="1" />
                            </div>
                            <div className="card-title col-7">
                                <h3>Username</h3>
                                <h5>review</h5>
                             </div>
                            <div className="footer col-3">
<h4>DATE</h4>
<h6> <i className="far fa-star"></i><i className="far fa-star"></i><i className="far fa-star"></i><i className="far fa-star"></i><i className="far fa-star"></i></h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
