import React, { Component } from 'react'


import NavEmp from '../component/navReg/NavEmp';
import NavFl from '../component/navReg/NavFl';
import '../css/link.css';
export default class Register extends Component {


    render() {
        const or = "หรือ";

        return (

            <div className="container mt-5">

                <div className="card-group">

                    <div class="card bg-dark text-white" >
                        <NavEmp tolink="/MainEmp" item="Emp" >
                            
                        </NavEmp>
                    </div>

                    <div className=" m-5 align-self-center"><hr/>หรือ <hr/></div>

                    <div class="card bg-dark text-white">
                    <NavFl tolink="/MainFl" item="Fl" >
                           
                        </NavFl>
                    </div>
                
                </div>
            </div>
        )
    }
}
