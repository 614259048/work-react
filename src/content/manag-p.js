import React, { Component } from 'react'
import logo from '../img/1.jpg'
import '../css/prof.css'
import { Link } from 'react-router-dom'
export default class ManagP extends Component {
    render() {
        return (
            <div className=" container mt-5 mb-5">
                <div className="">
                    <h3>
                        Manage Profile
                    </h3>
                </div>

                <div className="row justify-content-center">

                    <div className="col p-5">
                        <img src={logo} alt="" id="pic-profile" /><br />
                        <button type="submit" className="btn btn-danger btn-block mt-2">Change Photo</button>
                    </div>
                    <div className="col">
                        <div class="input-group mb-3">
                            <div class="input-group">
                                <span class="" id="basic-addon1"><b>Username</b></span>
                            </div>
                            <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1" />
                        </div>
                 
                   
                        <div class="input-group mb-3">
                            <div class="input-group">
                                <span class="" id="basic-addon1"><b>FirstName</b></span>
                            </div>
                            <input type="text" class="form-control" placeholder="Firstname" aria-label="Firstname" aria-describedby="basic-addon1" />
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group">
                                <span class="" id="basic-addon1"><b>Lastname</b></span>
                            </div>
                            <input type="text" class="form-control" placeholder="Lastname" aria-label="Lastname" aria-describedby="basic-addon1" />
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group">
                                <span class="" id="basic-addon1"><b> E-mail</b></span>
                            </div>
                            <input type="text" class="form-control" placeholder="E-mail" aria-label="E-mail" aria-describedby="basic-addon1" />
                            <button className="btn btn-success" >OTP</button>
                        </div>


                        <div class="input-group mb-3">
                            <div class="input-group">
                                <span class="" id="basic-addon1"><b>Phone</b></span>
                            </div>
                            <input type="text" class="form-control" placeholder="Phone" aria-label="Phone" aria-describedby="basic-addon1" />
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group">
                                <span class="" id="basic-addon1"><b>Birth</b></span>
                            </div>
                            <input type="date" class="form-control" placeholder="Birth" aria-label="Birth" aria-describedby="basic-addon1" />
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group">
                                <span class="" id="basic-addon1"><b>Address</b></span>
                            </div>
                            <textarea class="form-control" aria-label="Address" placeholder="Address" ></textarea>
                        </div>
                        <button className="btn btn-danger btn-block">Save</button>
                    </div>
                </div>
            </div>
        )
    }
}
