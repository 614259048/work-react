import React,{useState} from "react";
import Categories from '../component/Categories';
import Menu from './Menu';
import items from '../data';

const showdata =[... new Set (items.map((item)=> item.type))];


const Cat = () => {
  const [menuItems,setMenuItems] = useState(items);
  
  const filterItems = (type)=>{
    const newItems = items.filter((item) => item.type === type) ;
  setMenuItems(newItems);
  }

  return (
    <main>
      <section className="menu section">
        <Categories showdata={showdata} filterItems={filterItems}/>
        <Menu items={menuItems}/>
      </section>
    </main>
  );
};

export default Cat;
