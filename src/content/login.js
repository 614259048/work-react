import React, { Component } from 'react'

export default class Login extends Component {
    render() {
        return (
            <div className="container p-5 posi-reg">
            <div className="card-regis card shadow">
                <h3>SIGN UP</h3>
                <form>
                    <div className="mb-3">
                        <label for="exampleInputEmail1" className="form-label">UserName</label>
                        <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"   />

                    </div>

                    <div className="mb-3">
                        <label for="exampleInputEmail1" className="form-label">Password</label>
                        <input type="password" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />

                    </div>
                 
                    <div className="mt-5 d-grid gap-2 d-md-flex justify-content-md-end">
                        <button className="btn btn-primary me-md-2" type="submit">Sign Up</button>
                        <button className="btn btn-light" type="button">cancel</button>
                    </div>
                </form>
            </div>
        </div>
        )
    }
}
