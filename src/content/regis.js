import React, { Component } from 'react'
import axios from 'axios'
import api from '../Codeig_api'
import PasswordStrengthBar from 'react-password-strength-bar';
import Swal from 'sweetalert2'
export default class Regis extends Component {

    constructor() {
        super();
        this.state = {
            password: "",
            scors: "",
            chkuser: false,
        }
    }

    chkpass = () => {
        this.setState({ password: this.getpassword.value })
    }
    chkuser(e) {
        if (e.target.value.match(/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$/)) {
            this.setState({ chkuser: false });
        } else {
            this.setState({ chkuser: true });
        }
    }
    send = (e) => {
        e.preventDefault();
        var err = "";
        const username = this.getusername.value;
        const password = this.getpassword.value;
        const chkpassword = this.getchkpassword.value;
        const fname = this.getfname.value;
        const lname = this.getlname.value;

        if (!username) {
            Swal.fire(
                'Not username?',
                'Please enter username',
                'warning',
                document.regis.username.focus()
            )
            return false;
        }
        if (username.length < 5) {
            Swal.fire(
                'Username not long enough?',
                'Please enter username',
                'warning',
                document.regis.username.focus()
            )
            return false;
        }
        if (/[!@#$%^&*()_+\-={};':"|,.<>?]/.test(username)) {
            Swal.fire(
                'Don`t enter special characters?',
                'Please enter username',
                'warning',
                document.regis.username.focus()
            )

            return false;
        }

        if (this.state.chkuser === true) {

            // err = "Have at least 1 number or least 1 character in username"
            // document.getElementById('user').innerHTML = err;
            Swal.fire(
                'Have at least 1 number or least 1 character in username',
                'Please enter username',
                'warning',
                document.regis.username.focus()
            )

            return false;
        }
        if (!fname) {
            // err = "Please enter your first name"
            // document.getElementById('fname').innerHTML = err
            Swal.fire(
                'Not Firstname?',
                'Please enter your first name',
                'warning',
                document.regis.firstname.focus()
            )

            return false;
        }
        if (!lname) {
            // err = "Please enter last name"
            // document.getElementById('lname').innerHTML = err
            Swal.fire(
                'Not Lastname?',
                'Please enter last name',
                'warning',
                document.regis.lastname.focus()
            )

            return false;
        }

        if (!password) {
            // err = "Please enter password"
            // document.getElementById('password').innerHTML = err
            Swal.fire(
                'Not Password?',
                'Please enter password',
                'warning',
                document.regis.pass.focus()
            )

            return false;
        }
        if (this.state.scors <= 2) {
            // err = "Password is not secure"
            // document.getElementById('password-s').innerHTML = err
            Swal.fire(
                'Password is not secure?',
                'Please enter password',
                'warning',
                document.regis.pass.focus()
            )

            return false;
        }
        if (!chkpassword) {
            // err = "Please enter password"
            // document.getElementById('password-c').innerHTML = err
            Swal.fire(
                'Passwords not match?',
                'Please enter password?',
                'warning',
                document.regis.chkpass.focus()
            )

            return false;
        }

        if (password !== chkpassword) {
            // err = "Please enter the password exactly"
            // document.getElementById('password-c').innerHTML = err
            Swal.fire(
                'Passwords not match?',
                'Please enter password?',
                'warning',
                document.regis.chkpass.focus()
            )

            return false;
        }
        else {
            axios.post(api('register'),
                JSON.stringify({

                    'username': username,
                    'password': password,
                    'fname': fname,
                    'lname': lname


                }))
                .then(res => {
                    if (res.data === 'success') {
                        // alert('Successfully registered')
                        Swal.fire(
                            'The Internet?',
                            'That thing is still around?',
                            'warning',
                            window.location.reload()
                        )

                    }
                    else {
                        Swal.fire(
                            'The Internet?',
                            'That thing is still around?',
                            'warning'

                        )
                        // alert('Have this username already')

                    }
                })

        }

    }


    render() {
        return (

            <div className="container p-5 posi-reg">

                <div className="card-regis card shadow">
                    <h3>REGISTER</h3>
                    <form name="regis" onSubmit={this.send}>
                        <div className="mb-3">
                            <label for="exampleInputEmail1" className="form-label">UserName</label>
                            <input type="text" name="username" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" onChange={this.chkuser.bind(this)} ref={(input) => this.getusername = input} />
                            <span id="user" className="error"></span>
                        </div>
                        <div className="mb-3">
                            <label for="exampleInputEmail1" className="form-label">FistName</label>
                            <input type="text" name="firstname" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" ref={(input) => this.getfname = input} />
                            <span id="fname" className="error"></span>
                        </div>
                        <div className="mb-3">
                            <label for="exampleInputEmail1" className="form-label">LastName</label>
                            <input type="text" name="lastname" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" ref={(input) => this.getlname = input} />
                            <span id="lname" className="error"></span>
                        </div>
                        <div className="mb-3">
                            <label for="exampleInputPassword1" className="form-label" >Password</label>
                            <input type="password" name="pass" className="form-control" id="exampleInputPassword1" onChange={this.chkpass} ref={(input) => this.getpassword = input} />

                            <span id="password" className="error"></span>
                            <PasswordStrengthBar password={this.state.password} onChangeScore={score => {
                                this.setState({ scors: score })
                            }} />
                            <span id="password-s" className="error"></span>
                        </div>
                        <div className="mb-3">
                            <label for="exampleInputPassword1" className="form-label">confirm Password</label>
                            <input type="password" name="chkpass" className="form-control" id="exampleInputPassword1" ref={(input) => this.getchkpassword = input} />
                            <span id="password-c" className="error"></span>
                        </div>

                        <div className="mt-5 d-grid gap-2 d-md-flex justify-content-md-end">
                            <button name="sub" className="btn btn-primary me-md-2" type="submit">Register</button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

